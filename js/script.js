const button = document.querySelector('.button7');
const main = document.querySelector('main');

main.className = localStorage.getItem('theme');

button.addEventListener('click', function() {
    if (localStorage.getItem('theme') === 'container') {
        main.className = 'container_light';
        localStorage.setItem('theme', 'container_light')
    }
    else {
        main.className = 'container';
        localStorage.setItem('theme', 'container')
    }
})